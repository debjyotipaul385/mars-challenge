#!/bin/python
# Gateway Tier: It takes sensor information, logs to the console and passes that information to the Game Contoller
# Requires the following library to install: sudo pip install websocket-client
# if you encounter errors with a Six import:
# you can try: pip remove six; pip install six
import time
import logging
import logging.config
import os
import json
import math
import collections
import websocket
import requests

import settings
import pymongo

# Logging Initialization
logging.config.dictConfig(settings.GATEWAY_LOGGING)
logger = logging.getLogger("root")


# Global Variables
sensor_endpoints = os.environ['SENSOR_ENDPOINT'].split(',')
controller_endpoint = os.environ['GC_ENDPOINT']



controller_url = 'http://' + controller_endpoint + '/api/readings'  #'http://localhost:8080/api/readings'
admin_password = os.environ['ADMIN_PASS']
auth_header = {'X-Auth-Token': admin_password}

logger.info("Sensor EndPoint: {0}.".format(controller_url))

db=None

def get_mongoconn():
    global db
    try:
        conn=pymongo.MongoClient('107.170.244.17',27017)
        db = conn.backup.readings
        print "Connected successfully!!!"
    except pymongo.errors.ConnectionFailure, e:
        print "Could not connect to MongoDB: %s" % e 

def rename_endpoints(sensor_endpoints):
    sensor_sockets=[]
    for sensor_endpoint  in sensor_endpoints:
        sensor_sockets.append('ws://' + sensor_endpoint +'/ws')  # "ws://localhost:8085/ws"
        logger.info("Game Controller EndPoint: {0}.".format(sensor_endpoint))
    return sensor_sockets



def get_socket(sensor_socket):
    #Check that the Sensor is ready to serve weather data
    while True:
        try:
            logger.info("Trying to connect to Sensor Socket: {0:s}".format(sensor_socket))
            conn= websocket.create_connection(sensor_socket)
            logger.info("GOT connection")
            return conn
        except Exception as serror:
            logger.info("Error (Sensor Socket): {}".format(str(serror.message)))
            time.sleep(5)
            continue

def get_sockets(sensor_endpoints):
    ws = []
    for sensor_endpoint in sensor_endpoints:
        ws.append(get_socket(sensor_endpoint))
    return ws


# curl -i -H 'X-Auth-Token: 1234' -X POST -d '{"solarFlare":true,"temperature":-100,"radiation":384}' http://localhost:8080/api/readings
def push_data(data):
    db.insert(data)


def run(ws):
    # Start collecting the Sensor Data and sending the data to the Game Server
    while True:
        # Get Data from the Sensor Websocket
        sensor_data=[]
        solarFlare = False
        stamp = None
        temp_sum = 0.0 
        radiation_sum = 0.0
        cnt = 0
	for each_socket in ws:
            result = each_socket.recv()
            logger.info(str(result))
            mod_result = result
            result = json.loads(result)
            mod_result = result
            temp_sum +=result['temperature']
            radiation_sum +=result['radiation']
            if result['solarFlare']:
                cnt = cnt + 1
            if cnt > 2:
                solarFlare = True
            # solarFlare= solarFlare or result['solarFlare']
            stamp = result['stamp']
            sensor_data.append(result)
            mod_result['source'] = "sensor"
            push_data(mod_result)

        temperature_avg = temp_sum /len(ws)
        radiation_avg = int(radiation_sum/len(ws))
        
        data =  {}
        #data['stamp'] = stamp
        data['solarFlare'] = solarFlare
        data['temperature'] = temperature_avg
        data['radiation'] = radiation_avg
       	print json.dumps(data) 
	try:
            response = requests.post(url=controller_url, headers=auth_header, data=json.dumps(data))
            if response.status_code == 200:
                logger.info("Data has been accepted!")
                logger.info("Sending: {0:s}".format(json.dumps(data)))
                logger.info("HTTP Code: {0}  | Response: {1}".format(str(response.status_code), response.text))
        
            else:
                logger.error("We got an Error")
                logger.error("HTTP Code: {0}  | Response: {1}".format(str(response.status_code), response.text))
        except:
            pass
        data['source']="avg"
        push_data(data)
def push_data_mongo(data):
    client = MongoClient()
    db = client.test            

def close_socket(socket):
    if socket is None:
        print "NO CONNECTION TO CLOSE "
        return
    socket.close()
def close_sockets(ws): 
    for socket in ws:
        close_socket(socket)

if __name__=="__main__":
    get_mongoconn()
    sensor_endpoints = rename_endpoints(sensor_endpoints)
    ws = get_sockets(sensor_endpoints)
    run(ws)
    close_socket(ws)

